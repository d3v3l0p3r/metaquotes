﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Metaquotes.DAL.Exceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace MetaQuotes.WebApi.Middlewares
{
    public class ExceptionsMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (EntityNotFoundException e)
            {
                var result = JsonConvert.SerializeObject(new {error = e.Message});
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = StatusCodes.Status404NotFound;
                await context.Response.WriteAsync(result);
            }
            catch (Exception e)
            {
                var result = JsonConvert.SerializeObject(new { error = e.Message });
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                await context.Response.WriteAsync(result);
            }
        }
    }
}
