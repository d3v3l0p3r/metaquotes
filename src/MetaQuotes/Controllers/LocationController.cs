﻿using System;
using System.Threading.Tasks;
using MetaQuotes.Domain.Services.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace MetaQuotes.WebApi.Controllers
{
    [ApiController]
    public class LocationController : ControllerBase
    {
        private readonly ILocationService _locationService;

        public LocationController(ILocationService locationService)
        {
            _locationService = locationService;
        }

        /// <summary>
        /// Запрос местоположения по ip адресу
        /// </summary>
        /// <param name="ip">ip адрес</param>
        [HttpGet]
        [Route("ip/[action]")]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = Int32.MaxValue)] //Данные не меняются, поэтому кеш постоянный 
        public async Task<IActionResult> Location(string ip)
        {
            var location = _locationService.GetByIpAddress(ip);

            return Ok(location);
        }

        /// <summary>
        /// Поиск по названию города
        /// </summary>
        /// <param name="city">Название города</param>
        /// <returns></returns>
        [HttpGet]
        [Route("city/[action]")]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = Int32.MaxValue)] //Данные не меняются, поэтому кеш постоянный 
        public async Task<IActionResult> Locations(string city)
        {
            var location = _locationService.GetByCityName(city);

            return Ok(location);
        }
    }
}