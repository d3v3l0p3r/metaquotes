﻿var router = {
    routes: {
        "/": { title: 'Поиск по IP', viewName: 'ip_search', visible: false },
        "#ip_search": { title: 'Поиск по IP', viewName: 'ip_search', visible: true },
        "#city_search": { title: 'Поиск названию города', viewName: 'city_search', visible: true },
    },
    init: function () {  //Инициализация роутера. Маппинг путей на колбеки загрузки страниц. 
        this._routes = [];
        var routes = this.routes;
        for (var route in routes) {
            if (routes.hasOwnProperty(route)) {
                this.fillNav(route, routes[route]);
                var viewName = routes[route].viewName;
                this._routes.push({
                    pattern: new RegExp('^' + route.replace(/:\w+/, '(\\w+)') + '$'),
                    callback: this.setView,
                    route: viewName
                });
            }
        }
    },
    fillNav: function (path, route) { //Заполнить панель навигации
        if (route.visible) {
            var menu = document.getElementById("sidebar");
            var menuItem = document.createElement("a");
            var that = this;

            menuItem.innerHTML = route.title;
            menuItem.setAttribute('href', path);
            menuItem.addEventListener('click', function () {
                that.nav(path); //колбек по клику на навигации
            });
            menu.appendChild(menuItem);

        }
    },
    nav: function (path) { // переход по роутингу
        var i = this._routes.length;
        while (i--) {
            var args = path.match(this._routes[i].pattern);
            if (args) {
                var c = this._routes[i];
                c.callback.apply(this, [c.route, args.slice(1)]);
            }
        }
    },
    setView: function (route) { // Установка текущей страницы
        var routerView = document.getElementById('router-view');
        routerView.innerHTML = "";
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) { // Вставить в router-view
                    var appendElement = document.createRange().createContextualFragment(this.responseText);
                    routerView.appendChild(appendElement);
                }
                if (this.status === 404) {
                    routerView.innerHTML = "<h1>Страница не найдена.</h1>";
                }
            }
        }
        request.open("GET", route + '.html', true); //Не обошлось без соглашений. Имя файла должно совпадать с именем маршрута.
        request.send();
    }
}

function formatResult(json, elementId) {
    var resultElement = document.getElementById(elementId);
    var resultString = "";
    for (var p in json) {
        if (json.hasOwnProperty(p)) {
            resultString += p + ': ' + json[p] + '\n';
        }
    }

    resultElement.value = resultString;
}


document.addEventListener('DOMContentLoaded', function () {
    router.init();
    router.nav('/');
});
