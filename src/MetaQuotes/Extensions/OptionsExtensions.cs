﻿using Metaquotes.DAL.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MetaQuotes.WebApi.Extensions
{
    public static class OptionsExtensions
    {
        public static IServiceCollection AddOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<DalOptions>(configuration.GetSection(nameof(DalOptions)));


            return services;
        }
    }
}
