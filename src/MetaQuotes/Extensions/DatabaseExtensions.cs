﻿using System.IO;
using Metaquotes.DAL.Options;
using Metaquotes.DAL.Services.Abstractions;
using Metaquotes.DAL.Services.Implementations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MetaQuotes.WebApi.Extensions
{
    public static class DatabaseExtensions
    {
        public static IServiceCollection AddBinaryDatabase(this IServiceCollection services)
        {
            services.AddTransient<IBinaryDatabaseFiller, BinaryDatabaseFiller>();
            services.AddSingleton<IBinaryDatabase, BinaryDatabase>();
            return services;
        }

        public static IApplicationBuilder UseBinaryDatabase(this IApplicationBuilder app, IWebHostEnvironment environment)
        {
            var reader = app.ApplicationServices.GetService<IBinaryDatabaseFiller>();
            var options = app.ApplicationServices.GetService<IOptions<DalOptions>>();

            var path = Path.Combine(environment.WebRootPath, options.Value.SourceFilePath);
            reader.Fetch(path);

            return app;
        }
    }
}
