﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Metaquotes.DAL.Entities;
using Metaquotes.DAL.Exceptions;
using Metaquotes.DAL.Services.Abstractions;

namespace Metaquotes.DAL.Services.Implementations
{
    public class BinaryDatabase : IBinaryDatabase
    {
        private List<AddressRange> _addressRanges;
        private List<Location> _locations;
        private List<int> _cityIndexList;
        private int _version;
        private string _prefix;
        private DateTimeOffset _createTime;
        private List<IndexNested> _indexJoin;

        public void SetVersion(int version)
        {
            _version = version;
        }

        public void SetPrefix(string prefix)
        {
            _prefix = prefix;
        }

        public void SetCreateTime(ulong timestamp)
        {
            _createTime = DateTimeOffset.FromUnixTimeSeconds((long)timestamp);
        }

        public void SetAddressRangeList(List<AddressRange> list)
        {
            _addressRanges = list;
        }

        public void SetLocationsList(List<Location> list)
        {
            _locations = list;
        }

        public void SetCityIndexList(List<int> list)
        {
            _cityIndexList = list;
        }

        public Location FindLocationIndexByIp(uint ipAddress)
        {
            var range = BinarySearchAddressRange(ipAddress);

            return _locations[(int)range.LocationIndex];
        }

        public Location FindLocationByCityName(string city)
        {
            var bytes = new byte[24];
            var cityNameBytes = Encoding.UTF8.GetBytes(city);
            for (int i = 0; i < cityNameBytes.Length; i++)
            {
                bytes[i] = cityNameBytes[i];
            }

            return BinarySearchByCityIndex(bytes);
        }

        private AddressRange BinarySearchAddressRange(uint ipAddress)
        {
            int low = 0;
            int high = _addressRanges.Count;

            while (low <= high)
            {
                var mid = (low + high) / 2;
                if (mid == low || mid == high)
                {
                    break;
                }

                var range = _addressRanges[mid];

                if (range.From <= ipAddress && ipAddress <= range.To)
                {
                    return range;
                }

                if (range.To > ipAddress)
                {
                    high = mid;
                }

                if (range.To < ipAddress)
                {
                    low = mid;
                }
            }

            throw new EntityNotFoundException("Не удалось найти промежуток адресов");
        }

        private void LoadIndexJoin()
        {
            var joinList = (from index in _cityIndexList
                join location in _locations on index equals (int)location.Offset
                select new IndexNested { Index = index, Location = location }).ToList();
            _indexJoin = joinList;
        }

        private Location BinarySearchByCityIndex(byte[] cityName)
        {
            int low = 0;
            int high = _cityIndexList.Count;

            var cityNameString = Encoding.UTF8.GetString(cityName);
            if (_indexJoin == null)
            {
                LoadIndexJoin();
            }

            while (low <= high)
            {
                var mid = (low + high) / 2;
                if (mid == low || mid == high)
                {
                    break;
                }

                var index = _indexJoin[mid];
                var midCity = Encoding.UTF8.GetString(index.Location.City);

                if (midCity == cityNameString)
                {
                    return index.Location;
                }

                for (int i = 4; i < 24; i++)
                {
                    if (midCity[i] > cityName[i])
                    {
                        high = mid;
                        break;
                    }

                    if (midCity[i] < cityName[i])
                    {
                        low = mid;
                        break;
                    }
                }
            }

            throw new EntityNotFoundException("Не удалось найти город");
        }

        private class IndexNested
        {
            public int Index { get; set; }

            public Location Location { get; set; }
        }
    }
}
