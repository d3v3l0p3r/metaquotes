﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Metaquotes.DAL.Entities;
using Metaquotes.DAL.Services.Abstractions;

namespace Metaquotes.DAL.Services.Implementations
{
    public class BinaryDatabaseFiller : IBinaryDatabaseFiller
    {
        private readonly IBinaryDatabase _binaryDatabase;

        public BinaryDatabaseFiller(IBinaryDatabase binaryDatabase)
        {
            _binaryDatabase = binaryDatabase;
        }

        public void Fetch(string path)
        {
            if (!File.Exists(path))
            {
                throw new Exception("Файл базы данных не найден");
            }

            using (var reader = new BinaryReader(File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read)))
            {
                var encoding = Encoding.UTF8;

                var version = reader.ReadInt32();
                _binaryDatabase.SetVersion(version);

                var prefixName = reader.ReadBytes(32);
                _binaryDatabase.SetPrefix(encoding.GetString(prefixName));

                var timestamp = reader.ReadUInt64();
                _binaryDatabase.SetCreateTime(timestamp);

                var recordsCount = reader.ReadInt32();
                var offsetRanges = reader.ReadUInt32();
                var offsetCities = reader.ReadUInt32();
                var offsetLocations = reader.ReadUInt32();

                var ranges = LoadRanges(reader, offsetRanges, recordsCount);
                _binaryDatabase.SetAddressRangeList(ranges);

                var locations = LoadLocations(reader, offsetLocations, recordsCount);
                _binaryDatabase.SetLocationsList(locations);

                var indexList = ReadIndexes(reader, offsetCities, recordsCount);
                _binaryDatabase.SetCityIndexList(indexList);
            }
        }

        public List<AddressRange> LoadRanges(BinaryReader reader, uint start, int records)
        {
            var list = new List<AddressRange>(records);
            var currentPosition = start;
            var objectSize = 12;
            var end = start + records * objectSize;

            reader.BaseStream.Seek(start, SeekOrigin.Begin);

            while (currentPosition + objectSize <= end)
            {
                var range = new AddressRange()
                {
                    From = reader.ReadUInt32(),
                    To = reader.ReadUInt32(),
                    LocationIndex = reader.ReadUInt32()
                };

                list.Add(range);
                currentPosition += (uint)objectSize;
            }

            return list;
        }

        public List<Location> LoadLocations(BinaryReader reader, uint start, int records)
        {
            var list = new List<Location>(records);
            var currentPosition = start;
            var objectSize = 96;
            var end = start + records * objectSize;

            reader.BaseStream.Seek(start, SeekOrigin.Begin);

            while (currentPosition + objectSize <= end)
            {
                var location = new Location
                {
                    Country = reader.ReadBytes(8),
                    Region = reader.ReadBytes(12),
                    Postal = reader.ReadBytes(12),
                    City = reader.ReadBytes(24),
                    Organization = reader.ReadBytes(32),
                    Latitude = reader.ReadSingle(),
                    Longitude = reader.ReadSingle(),
                    Offset = currentPosition - start
                };

                list.Add(location);
                currentPosition += (uint)objectSize;
            }

            return list;
        }

        private List<int> ReadIndexes(BinaryReader reader, uint start, int records)
        {
            var list = new List<int>(records);
            var currentPosition = start;
            var objectSize = 4;
            var end = start + records * objectSize;

            while (currentPosition + objectSize <= end)
            {
                var index = reader.ReadInt32();
                list.Add(index);
                currentPosition += (uint)objectSize;
            }

            return list;
        }

    }
}
