﻿namespace Metaquotes.DAL.Services.Abstractions
{
    public interface IBinaryDatabaseFiller
    {
        void Fetch(string path);
    }
}