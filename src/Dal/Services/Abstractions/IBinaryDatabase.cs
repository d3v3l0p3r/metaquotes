﻿using System.Collections.Generic;
using Metaquotes.DAL.Entities;

namespace Metaquotes.DAL.Services.Abstractions
{
    public interface IBinaryDatabase
    {
        void SetVersion(int version);
        void SetPrefix(string prefix);
        void SetCreateTime(ulong timestamp);
        void SetAddressRangeList(List<AddressRange> list);
        void SetLocationsList(List<Location> list);
        void SetCityIndexList(List<int> list);

        Location FindLocationIndexByIp(uint ipAddress);
        Location FindLocationByCityName(string city);
    }
}