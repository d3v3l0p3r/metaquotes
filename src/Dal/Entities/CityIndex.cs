﻿namespace Metaquotes.DAL.Entities
{
    /// <summary>
    /// Индекс
    /// </summary>
    public class CityIndex
    {
        /// <summary>
        /// Индекс 
        /// </summary>
        public int Index { get; set; }
    }
}
