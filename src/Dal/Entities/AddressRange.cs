﻿namespace Metaquotes.DAL.Entities
{

    /// <summary>
    /// Интервал адресов
    /// </summary>
    public class AddressRange 
    {
        /// <summary>
        /// Начальный адрес
        /// </summary>
        public uint From { get; set; }

        /// <summary>
        /// Конечный адрес
        /// </summary>
        public uint To { get; set; }

        /// <summary>
        /// Индекс записи о местоположении
        /// </summary>
        public uint LocationIndex { get; set; }
    }
}
