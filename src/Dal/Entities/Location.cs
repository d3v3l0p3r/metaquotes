﻿using System.Text;

namespace Metaquotes.DAL.Entities
{
    /// <summary>
    /// Местоположение
    /// </summary>
    public class Location
    {
        /// <summary>
        /// Страна
        /// </summary>
        public byte[] Country { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public byte[] Region { get; set; }

        /// <summary>
        /// Индекс
        /// </summary>
        public byte[] Postal { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        public byte[] City { get; set; }

        /// <summary>
        /// Организация
        /// </summary>
        public byte[] Organization { get; set; }

        /// <summary>
        /// Широта
        /// </summary>
        public float Latitude { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public float Longitude { get; set; }

        /// <summary>
        /// Смещение в файле
        /// </summary>
        public uint Offset { get; set; }

        public override string ToString()
        {
            var encoding = Encoding.UTF8;

            return $"{encoding.GetString(Country).Normalize().Trim()}-{encoding.GetString(Region).Normalize().Trim()}-{encoding.GetString(Postal).Normalize().Trim()}-{encoding.GetString(City).Normalize().Trim()}";
        }
    }
}
