﻿using System;
using System.Collections.Generic;
using System.Text;
using MetaQuotes.Domain.Services.Abstractions;
using MetaQuotes.Domain.Services.Implementations;
using Microsoft.Extensions.DependencyInjection;

namespace MetaQuotes.Domain.Extensions
{
    public static class DomainServiceExtensions
    {
        public static IServiceCollection RegisterDomainServices(this IServiceCollection services)
        {
            services.AddTransient<ILocationService, LocationService>();

            return services;
        }
    }
}
