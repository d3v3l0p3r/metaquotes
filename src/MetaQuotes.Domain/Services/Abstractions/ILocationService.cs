﻿using Metaquotes.DAL.Entities;
using MetaQuotes.Domain.Models;

namespace MetaQuotes.Domain.Services.Abstractions
{
    public interface ILocationService
    {
        LocationModel GetByIpAddress(string ip);
        LocationModel GetByCityName(string city);
    }
}