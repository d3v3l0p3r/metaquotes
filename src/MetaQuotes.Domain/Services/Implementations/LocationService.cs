﻿using System;
using System.Net;
using System.Text;
using Metaquotes.DAL.Entities;
using Metaquotes.DAL.Services.Abstractions;
using MetaQuotes.Domain.Models;
using MetaQuotes.Domain.Services.Abstractions;

namespace MetaQuotes.Domain.Services.Implementations
{
    public class LocationService : ILocationService
    {
        private readonly IBinaryDatabase _database;

        public LocationService(IBinaryDatabase database)
        {
            _database = database;
        }


        public LocationModel GetByIpAddress(string ip)
        {
            var ipAddress = ConvertAddressToUint(ip);
            var location = _database.FindLocationIndexByIp(ipAddress);

            return ConvertToDomainModel(location);
        }

        public LocationModel GetByCityName(string city)
        {
            var location = _database.FindLocationByCityName(city);
            return ConvertToDomainModel(location);
        }

        private LocationModel ConvertToDomainModel(Location location) //можно было использовать сторонний маппер
        {
            var encoding = Encoding.UTF8;

            var dest = new LocationModel
            {
                Country = encoding.GetString(location.Country),
                Region = encoding.GetString(location.Region),
                Postal = encoding.GetString(location.Postal),
                City = encoding.GetString(location.City),
                Organization = encoding.GetString(location.Organization),
                Latitude = location.Latitude,
                Longitude = location.Longitude
            };

            return dest;
        }

        private uint ConvertAddressToUint(string addressString)
        {
            IPAddress.TryParse(addressString, out IPAddress ipAddress);
            if (ipAddress == null)
            {
                throw new Exception("Введен невалидный ip адрес");
            }

            return BitConverter.ToUInt32(ipAddress.GetAddressBytes());
        }
    }
}
